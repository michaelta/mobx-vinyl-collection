import { observable, action, computed } from 'mobx';

class PokemonStore {
    @observable
    pokemon = '';

    @observable
    pokemonCollection = [];

    @action
    addPokemon = (pokemon) => {
        this.pokemonCollection.push(pokemon);
    };

    @computed
    get pokemonCount() {
        return this.pokemonCollection.length;
    }
}

const store = new PokemonStore();

export default store;
