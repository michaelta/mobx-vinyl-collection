import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import { Provider } from 'mobx-react';
import PokemonStore from './stores/PokemonStore';
import Main from './pages/Main';
import MyCollection from './pages/MyCollection';
import WantList from './pages/WantList';

class App extends Component {
    render() {
        return (
            <Provider PokemonStore={PokemonStore}>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route exact path="/collection" component={MyCollection} />
                    <Route exact path="wantlist" component={WantList} />
                </Switch>
            </Provider>
        );
    }
}

export default App;
