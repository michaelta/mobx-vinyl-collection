import React from 'react';
import PokedexFrame from '../components/Layout/PokedexFrame';
import SearchPokemon from '../components/scan/SearchPokemon';

const Main = () => {
    return (
        <PokedexFrame>
            <SearchPokemon />
        </PokedexFrame>
    );
};

export default Main;
