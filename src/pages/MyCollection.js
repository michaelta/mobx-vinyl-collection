import React from 'react';
import PokedexFrame from '../components/Layout/PokedexFrame';
import Collection from '../components/collection/Collection';

const MyCollection = () => {
    return (
        <PokedexFrame>
            <Collection />
        </PokedexFrame>
    );
};

export default MyCollection;
