import React from 'react';
import PokedexFrame from '../components/Layout/PokedexFrame';

const WantList = () => {
    return (
        <PokedexFrame>
            <h1>Want List</h1>
        </PokedexFrame>
    );
};

export default WantList;
