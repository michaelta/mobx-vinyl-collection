import React from 'react';
import Navbar from '../hardware/Navbar';
import Footer from '../hardware/Footer';

const PokedexFrame = (props) => {
    return (
        <>
            <Navbar />
            <main>{props.children}</main>
            <Footer />
        </>
    );
};

export default PokedexFrame;
