import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Form from '../wantList/Form';
import List from '../wantList/List';

@inject('PokemonStore')
@observer
class Collection extends Component {
    render() {
        const { PokemonStore } = this.props;
        const { pokemonCount, pokemon } = PokemonStore;

        let collection;
        if (pokemonCount !== 0) {
            collection = <List />;
        } else {
            collection = <p>Such an empty collection</p>;
        }

        return (
            <div>
                <h1>Number of pokemons on my want list: {pokemonCount}</h1>
                <Form />
                <p>The pokemon you are about to add: {pokemon}</p>
                <h2>Want list: </h2>
                {collection}
            </div>
        );
    }
}

export default Collection;
