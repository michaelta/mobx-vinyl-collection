import React from 'react';
import { observer, inject } from 'mobx-react';
import SinglePokemon from './SinglePokemon';

const List = inject('PokemonStore')(
    observer(({ PokemonStore }) => {
        const resetCollection = () => {
            PokemonStore.pokemonCollection = [];
        };
        return (
            <div>
                {PokemonStore.pokemonCollection.map((pokemon, index) => {
                    return (
                        <SinglePokemon
                            key={pokemon.id}
                            pokemon={pokemon.pokemon}
                            collection={PokemonStore.pokemonCollection}
                            pokemonIndex={index}
                            iOwnThis={PokemonStore.pokemonCollection[index].owned}
                        />
                    );
                })}
                <button onClick={resetCollection}>Reset collection</button>;
            </div>
        );
    })
);

export default List;
