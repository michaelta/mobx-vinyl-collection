import React from 'react';
import { observer, inject } from 'mobx-react';

const SinglePokemon = inject('PokemonStore')(
    observer(({ pokemon, collection, pokemonIndex, PokemonStore }) => {
        const removePokemon = (i) => {
            // With Mobx state is mutable so direct changes are allowed. No need to make a shallow copy like const arrayCopy = [...array] (?)
            collection.splice(i, 1);
        };

        const togglePossession = (i) => {
            const iOwnThis = PokemonStore.pokemonCollection;
            iOwnThis[i].owned = !iOwnThis[i].owned;
        };

        return (
            <li>
                <span
                    onClick={() => togglePossession(pokemonIndex)}
                    style={{ textDecoration: PokemonStore.pokemonCollection[pokemonIndex].owned ? 'line-through' : '' }}
                >
                    {pokemon}
                </span>
                <button onClick={() => removePokemon(pokemonIndex)}>delete pokemon</button>
            </li>
        );
    })
);

export default SinglePokemon;
