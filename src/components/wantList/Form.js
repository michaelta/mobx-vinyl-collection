import React from 'react';
import { nanoid } from 'nanoid';
import { inject, observer } from 'mobx-react';

const Form = inject('PokemonStore')(
    observer(({ PokemonStore }) => {
        const handleChange = (e) => {
            PokemonStore.pokemon = e.target.value;
        };

        const handleSubmit = (e) => {
            e.preventDefault();
            if (PokemonStore.pokemon !== '') {
                PokemonStore.addPokemon({
                    pokemon: PokemonStore.pokemon,
                    id: nanoid(),
                    owned: false,
                });
                PokemonStore.pokemon = '';
            }
        };

        return (
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Add a pokemon" onChange={handleChange} value={PokemonStore.pokemon} />
                <button type="submit">Add</button>
            </form>
        );
    })
);

export default Form;
