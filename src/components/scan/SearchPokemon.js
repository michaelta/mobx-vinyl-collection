import React, { useState } from "react";
import DisplayPokemon from "./DisplayPokemon";
import { useFetch } from "../../hooks/useFetch";
import { endPoints } from "../../api/config";

const SearchPokemon = () => {
  const [searchQuery, setSearchQuery] = useState("");

  const [
    {
      data: { name, sprites },
      isLoading,
      isError,
    },
    getData,
  ] = useFetch(`${endPoints.baseUrl}/pikachu`, { hits: [] });

  return (
    <>
      <form
        onSubmit={(e) => {
          getData(`${endPoints.baseUrl}/${searchQuery}`);
          e.preventDefault();
          setSearchQuery("");
        }}
      >
        <input
          type="text"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <button>Search Pokemon!</button>
      </form>

      <DisplayPokemon
        pokemon={name}
        image={sprites}
        error={isError}
        isLoading={isLoading}
      />
    </>
  );
};

export default SearchPokemon;
