import React from "react";
// import { CircularProgress } from "@material-ui/core";

const SearchPokemon = ({ pokemon, image, isLoading, error }) => {
  const loading = (
    <div>
      {/* <CircularProgress variant="determinate" value={100} /> */}
      Loading...
    </div>
  );
  return isLoading ? (
    loading
  ) : (
    <div>
      <h1>{pokemon}</h1>
      {image ? <img src={image.front_default} alt={pokemon} /> : null}
      {error && <div>Something went wrong ...</div>}
    </div>
  );
};

export default SearchPokemon;
